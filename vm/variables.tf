variable "template" {
  type        = string
  description = "Template to use for the creation of the VM"
  default     = ""
}

variable "pool" {
  type        = string
  description = "The pool to use"
  default     = ""
}

variable "name" {
  type        = string
  description = "The name of the VM"
  default     = ""
}

variable "hostname" {
  type        = string
  description = "The hostname of the VM"
  default     = ""
}

variable "gateway" {
  type        = string
  description = "The gateway of the VM"
  default     = "192.168.1.1"
}

variable "dns" {
  type        = string
  description = "The list of DNS of the VM, comma separeted"
  default     = "192.168.1.2,1.1.1.1"
}

variable "dns_domain" {
  type        = string
  description = "DNS domain" 
  default     = "homelab.local"
}

variable "cpu_arch" {
  type        = string
  description = "The architecture of the CPU to use"
  default     = "x86_64"
}

variable "machine_type" {
  type        = string
  description = "The machine type"
  default     = ""
}

variable "cpu_nb" {
  type        = number
  description = "The number of CPU"
  default     = 2
}

variable "ram" {
  type        = number
  description = "The sze of the RAM in MB"
  default     = 1024
}

variable "network" {
  type        = string
  description = "The name of the network to be used"
  default     = ""
}

variable "sda_size" {
  type        = number
  description = "The size of first disk in GB"
  default     = 16
}

variable "disks" {
  description = "List of the secondary disks"
  type = map(object({
    name = string
    size = number
  }))
}

variable "ip_v4" {
  type        = string
  description = "The IPv4 of the VM"
  default     = ""
}

