#################################################################
#                           DATA                                #
#################################################################
data "template_file" "user_data" {
  template = file("../cloud_init/user_data.cfg")
  vars = {
    HOSTNAME = var.hostname
  }
}

data "template_file" "network_config" {
  template = file("../cloud_init/network.cfg")
  vars = {
    IP = var.ip_v4
    GATEWAY = var.gateway
    DNS = var.dns
  }
}

#################################################################
#                         RESOURCES                             #
#################################################################
# Template to be used
resource "libvirt_volume" "os-root" {
  name   = "${var.name}-os.qcow2"
  pool   = var.pool
  source = var.template
}

# Create a new resized LV from the base LV
resource "libvirt_volume" "os-root-extended" {
  name           = "${var.name}-extended.qcow2"
  pool           = var.pool
  base_volume_id = libvirt_volume.os-root.id
  size           = var.sda_size * 1024 * 1024 * 1024
}

resource "libvirt_volume" "secondary_disks" {
  for_each = var.disks
  name = "${var.name}-${each.value.name}"
  size = each.value.size * 1024 * 1024 * 1024
  pool = var.pool
}

# Cloud-init
resource "libvirt_cloudinit_disk" "commoninit" {
  name           = "${var.name}-commoninit.iso"
  pool           = var.pool
  user_data      = data.template_file.user_data.rendered
  network_config = data.template_file.network_config.rendered
}


#################################################################
#                      VRITUAL MACHINE                          #
#################################################################
# Create the machine
resource "libvirt_domain" "vm" {
  autostart = true
  name      = var.name
  memory    = var.ram
  arch      = var.cpu_arch
  machine   = var.machine_type
  vcpu      = var.cpu_nb
  cloudinit = libvirt_cloudinit_disk.commoninit.id
  network_interface {
    network_name = var.network
  }

  # IMPORTANT
  # linux can hang is a isa-serial is not present at boot time.
  # If you find your CPU 100% and never is available this is why
  console {
    type        = "pty"
    target_port = "0"
    target_type = "serial"
  }

  console {
    type        = "pty"
    target_type = "virtio"
    target_port = "1"
  }

  disk {
    volume_id = libvirt_volume.os-root-extended.id
  }

  dynamic "disk" {
    iterator = disk
    for_each = libvirt_volume.secondary_disks
    content {
      volume_id = disk.value.id
    }
  }

  graphics {
    type        = "spice"
    listen_type = "address"
    autoport    = "true"
  }
}

# Output Server IP
output "ip" {
  value = "libvirt_domain.${var.name}.network_interface.0.addresses.0"
}
