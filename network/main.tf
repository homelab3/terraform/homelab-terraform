resource "libvirt_network" "networkd" {
  name = var.name
  mode = var.mode

  addresses = var.addresses
  domain    = var.domain

  autostart = var.autostart

  dhcp {
    enabled = false
  }

  dns {
    enabled = true

    local_only = false
    forwarders { address = "127.0.0.53" }
    #TODO map of object
    hosts  {
        hostname = "t1master1a"
        ip = "10.1.10.10"
      }
    hosts {
        hostname = "t1compute1a"
        ip = "10.1.10.11"
      }
    hosts {
        hostname = "t1compute1b"
        ip = "10.1.10.12"
      }
    hosts {
        hostname = "t1compute1c"
        ip = "10.1.10.13"
      }
  }
}
