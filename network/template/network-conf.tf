module "t1template1" {
  source       = "git@gitlab.com:homelab3/terraform/terraform-libvirt.git//network"
  providers = {
    libvirt = libvirt.local-libvirt
  }
  name        = "t10k3s_network"
  mode        = "nat"
  addresses   = ["10.0.10.1/28"]
  domain      = "k3s.local"
  autostart   = "true"
}
